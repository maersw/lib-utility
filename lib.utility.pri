include(qmakeUtils.pri)

contains(libUtil, qt){
    message("libUtil += qt")

    HEADERS += \
        $$PWD/src/utilqmlplugin.hpp \
        $$PWD/src/logging/logcomponent.hpp \
        $$PWD/src/logging/log2file.hpp
    
    SOURCES += \
        $$PWD/src/utilqmlplugin.cpp \
        $$PWD/src/logging/logcomponent.cpp \
        $$PWD/src/logging/log2file.cpp

    LIBUTIL_GEN_INCFILES += \
        $$PWD/src/utilqmlplugin.hpp \
        $$clean_path($$PWD/src/logging/log2file.hpp)

    DEFINES += LIBUTIL_QT=1    
}
else {
    message("libUtil -= qt")
    DEFINES += LIBUTIL_QT=0
}

contains(libUtil, kmp){
    message("libUtil += kmp")
    HEADERS += $$PWD/src/util_kmp.hpp
    SOURCES += $$PWD/src/util_kmp.cpp

    LIBUTIL_GEN_INCFILES += $$PWD/src/util_kmp.hpp
    DEFINES += LIBUTIL_KMP=1
}
else{
    message("libUtil -= kmp")
    DEFINES += LIBUTIL_KMP=0
}

contains(libUtil, keyboardSimulator){
    message("libUtil += keyboardSimulator")
    HEADERS += $$PWD/src/keyboardSimulator/util_keyboardsimulator.hpp
    SOURCES += $$PWD/src/keyboardSimulator/util_keyboardsimulator.cpp
    LIBUTIL_GEN_INCFILES += $$PWD/src/keyboardSimulator/util_keyboardsimulator.hpp

    DEFINES += LIBUTIL_KEYBOARDSIMULATOR=1

    contains(libUtil, qt){
        HEADERS += $$files($$PWD/src/keyboardSimulator/util_keyboardsimulatorcomponent.hpp)
        SOURCES += $$files($$PWD/src/keyboardSimulator/util_keyboardsimulatorcomponent.cpp)
    }
}
else{
    message("libUtil -= keyboardSimulator")
    DEFINES += LIBUTIL_KEYBOARDSIMULATOR=0
}


contains(libUtil, winTools){
    message("libUtil += winTools")
    HEADERS += \
        $$PWD/src/winTools/wintools.hpp \
        $$PWD/src/winTools/window.hpp \


    SOURCES += \
        $$PWD/src/winTools/wintools.cpp \
        $$PWD/src/winTools/window.cpp \

    LIBUTIL_GEN_INCFILES += $$PWD/src/winTools/wintools.hpp
    DEFINES += LIBUTIL_WINTOOLS=1
}
else{
    message("libUtil -= winTools")
    DEFINES += LIBUTIL_WINTOOLS=0
}

# Generate include files
libUtilIncGen.output = lib.utility.${QMAKE_FILE_BASE}.hpp
libUtilIncGen.commands = "echo '$${LITERAL_HASH}include \"${QMAKE_FILE_NAME}\"' > ${QMAKE_FILE_OUT}"
libUtilIncGen.input = LIBUTIL_GEN_INCFILES
libUtilIncGen.CONFIG += no_link target_predeps

QMAKE_EXTRA_COMPILERS += libUtilIncGen

