defineReplace(m_targetExt){
    
    win32:result=".exe"
    else:result=""

    return($$result)
}

defineReplace(m_targetPath){

    result="";

    win32{
        CONFIG(debug, debug|release){
            result="debug"
        }
        
        CONFIG(release, release|debug){
            result="release"
        }

        result="$$result\\$${TARGET}"$$m_targetExt()
    }
    else{
        result=$$TARGET
    }

    return($$result)
}

defineReplace(m_rmdirF){
    dir=$$ARGS

    result=""

    win32: result = "rmdir /Q /S $$dir || dir > nul"
    else: unix: result =  "rm -f $$dir"
    else: error("m_rmdirF: unsupported platform")

    result += $$escape_expand("\n\t")
    return($$result)
}

defineReplace(m_mkdir){
    dir=$$ARGS

    result=""

    win32: result = "mkdir $$dir"
    else: unix: result =  "mkdir $$dir"
    else: error("m_mkdir: unsupported platform");

    result += $$escape_expand("\n\t")

    result += "echo %cd%"$$escape_expand("\n\t")
    
    return($$result)
}

defineReplace(m_copy){
    file=$$1
    destinationDir=$$2

    result=""

    win32: result = "xcopy /S /I $$file $$destinationDir"
    else: unix: result =  "cp $$file $$destinationDir"
    else: error("m_mkdir: unsupported platform")

    result += $$escape_expand("\n\t")
    
    return($$result)
}

defineReplace(m_copyDir){
    sourceDir=$$1
    destinationDir=$$2

    result = ""

    win32:{
        src=$$replace(sourceDir, "/", "\\")
        dest=$$replace(destinationDir, "/", "\\")
        result = "xcopy $$src $$dest /e /s /i"
    }
    else: unix: result = "cp -pr $$sourceDir $$destinationDir"
    else: error("m_copyDir: unsupported platform")


    result += $$escape_expand("\n\t")
    
    return($$result)
}

defineReplace(m_winDeploy){
    targ=$$1

    result += $$m_rmdirF($$targ)
    result += $$m_mkdir($$targ)
    result += $$m_copy($$m_targetPath(), $$targ)
    CONFIG(qt): result += windeployqt --qmldir $$[QT_INSTALL_QML] "$$targ/$${TARGET}"$$m_targetExt()
    result += $$escape_expand("\n\t")

    return($$result)
}
