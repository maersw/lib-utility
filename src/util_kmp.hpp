/*
 * Copyright 2017 Martin Ertl
 * Copyright 2011 Shao-Chuan Wang <shaochuan.wang AT gmail.com>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
*/
#ifndef KMP_HPP
#define KMP_HPP

#include <string>
#include <vector>

namespace libUtil
{

template <class string_t>
/**
 * @brief The Kmp class implements the KMP algorithm.
 * The implementaion was inpired by
 * http://code.activestate.com/recipes/577908-implementation-of-knuthmorrispratt-algorithm/
 * with adaptions to support "stream processing", i. e. putting in 
 * on character after the other and get info when a specific pattern has been detected.
 * 
 * Example:
 * @code
std::string inputData = "Hello World, thillo s is foo";
std::string pattern = "llo";

Kmp<std::string> k(pattern);

for (auto iter = inputData.begin(); iter != inputData.end(); ++iter)
{
    if (k.putC(*iter))
    {
        std::cout<<"pattern found"<<std::endl;
    }
}
 * @endcode
 */
class Kmp
{
public:    
    /**
     * @brief Kmp
     * @param pattern This pattern will be searched
     */
    Kmp(const string_t & pattern):
      m_pattern(pattern)
      , m_k(0)
      , m_patternLength(pattern.size())
    {
        preKMP(pattern, m_f);
    }

    /**
     * @brief putC Feed one character after the other into this
     * method to check for the pattern specified in the constructor
     * @param chr Character
     * @return true: pattern specified in constructor has been detected
     *         false: else
     */
    bool putC(const typename string_t::value_type & chr)
    {
        int result = false;

        while (m_k > -1 && m_pattern[m_k+1] != chr)
        {m_k = m_f[m_k];}
        
        if (chr == m_pattern[m_k+1])
        {m_k++;}
        
        if (m_k == m_patternLength - 1) 
        {
            result = true;
            m_k = 0;
            //return i-k;
        }
        
        return result;
    }
    
    /**
     * @brief pattern
     * @return The pattern which will be used for matching
     */
    const string_t & pattern()const
    {return m_pattern;}
    
    /**
     * @brief setPattern updates the pattern
     * @param pattern
     */
    void setPattern(const string_t & pattern)
    {
        m_k = 0;
        m_pattern = pattern;
        m_patternLength = pattern.size();
        
        preKMP(pattern, m_f);
    }
    
private:
    string_t m_pattern;
    std::vector<int> m_f;
    int m_k;
    int m_patternLength;

    void preKMP(const string_t & pattern, std::vector<int> & f)
    {
        f.clear();
        f.resize(pattern.length(), 0);
        int k = -1;
        f[0] = k;
        
        unsigned int patternSize = pattern.size();
        
        for (unsigned int i = 1; i < patternSize; i++)
        {
            while (k > -1 && pattern[k+1] != pattern[i])
            {   
                k = f[k];
            }
            
            if (pattern[i] == pattern[k+1])
            {
                k++;
            }
            
            f[i] = k;
        }
    }
};

} // end namespace stringUtils
#endif // KMP_HPP
