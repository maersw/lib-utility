/*************************************************************************
 * Copyright (C) 2017 Martin Ertl Martin Ertl <ertl.martin@gmx.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "window.hpp"

#include <memory>

namespace libUtil
{
namespace winTools
{
Window::Window(HWND hwnd):
    m_hwnd(hwnd)
{
    
}

std::wstring Window::title() const
{
    int winTextLen = GetWindowTextLength(m_hwnd);
    
    std::shared_ptr<WCHAR> tempTitle = std::shared_ptr<WCHAR>(new WCHAR[winTextLen + 1], std::default_delete<WCHAR[]>());
    GetWindowText(m_hwnd, tempTitle.get(), winTextLen + 1);
    
    std::wstring title(tempTitle.get(), winTextLen);
    return title;
}

HWND Window::hwnd() const
{
    return m_hwnd;
}

}}
