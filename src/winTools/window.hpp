/*************************************************************************
 * Copyright (C) 2017 Martin Ertl Martin Ertl <ertl.martin@gmx.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <string>
#include <Windows.h>

namespace libUtil
{
namespace winTools
{
/**
 * @brief The Window class represents a very basic window.
 */
class Window
{
public:
    
    /**
     * @brief Window
     * @param hwnd window handle
     */
    Window(HWND hwnd);
    
    /**
     * @brief title
     * @return the window title
     */
    std::wstring title() const;
    
    /**
     * @brief hwnd
     * @return the window handle
     */
    HWND hwnd() const;
    
private:
    HWND m_hwnd;
    std::wstring m_title;
    
};

}}

#endif // WINDOW_HPP
