/*************************************************************************
 * Copyright (C) 2017 Martin Ertl Martin Ertl <ertl.martin@gmx.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "wintools.hpp"
#include <memory>

namespace libUtil
{
namespace winTools
{

struct enumWindowData
{
    compareFunc compFunc;
};
static BOOL CALLBACK enumWindowsProcCompFunc(_In_ HWND   hwnd, _In_ LPARAM lParam);


bool findWindow(compareFunc compFunc)
{
    enumWindowData data;
    data.compFunc  = compFunc;
    bool result = (EnumWindows(&enumWindowsProcCompFunc, reinterpret_cast<LPARAM>(&data)) != FALSE);
    return result;
}

BOOL enumWindowsProcCompFunc(HWND hwnd, LPARAM lParam)
{
    enumWindowData * data = reinterpret_cast<enumWindowData*>(lParam);
    
    std::shared_ptr<Window> win = std::make_shared<Window>(hwnd);
    
    bool continueEnum = data->compFunc(win);
    
    return continueEnum;
}

std::wstring getLastErrorString()
{
    // https://msdn.microsoft.com/de-de/library/windows/desktop/ms680582(v=vs.85).aspx
    LPVOID lpMsgBuf;
    DWORD dw = GetLastError(); 
    
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );
    
    LPCTSTR message = (LPCTSTR)lpMsgBuf;
    int strLen = lstrlen(message);
    
    std::wstring result(message, strLen);
    
    LocalFree(lpMsgBuf);
    
    return result;
}

}
}

