/*************************************************************************
 * Copyright (C) 2017 Martin Ertl Martin Ertl <ertl.martin@gmx.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#ifndef WINTOOLS_HPP
#define WINTOOLS_HPP

#include <string>
#include <memory>
#include <list>
#include <functional>

#include <Windows.h>

#include "window.hpp"

namespace libUtil
{
namespace winTools
{
    typedef std::function<bool(const std::shared_ptr<Window> & win)> compareFunc;

    /**
     * @brief findWindow wraps
     * [EnumWindows](https://msdn.microsoft.com/en-us/library/windows/desktop/ms633497(v=vs.85).aspx)
     * and calls a function for every detected window. 
     * 
     * Usage example:
     * @code
     * #include <list>
     * #include "lib.utility.wintools.hpp"
     * int main()
     * {
     *     std::list<std::shared_ptr<libUtil::winTools::Window>> windowsWithFooInTitle;
     *     libUtil::winTools::findWindow([&](const std::shared_ptr<libUtil::winTools::Window> & win){
     *         
     *         if (win->title().find(L"Foo") != std::wstring::npos)
     *         {
     *             windowsWithFooInTitle.push_back(win);
     *         }
     *         
     *         bool proceed = true;
     *         return proceed;
     *     });
     *     
     *     if (!windowsWithFooInTitle.empty())
     *     {
     *         // do something with detected windows 
     *     }
     * };
     * @endcode
     * @param compFunc This function gets called for each detected window. If the function
     * returns 'false' no more windows are enumerated, if the function returns 
     * 'true' the function will be called for the next window until all windows were
     * processed.
     * @return true on success, false on error
     */
    bool findWindow(compareFunc compFunc);

    /**
     * @brief getLastErrorString calls 
     * [GetLastError](https://msdn.microsoft.com/en-us/library/windows/desktop/ms679360(v=vs.85).aspx)
     * and 
     * [FormatMessage](https://msdn.microsoft.com/en-us/library/windows/desktop/ms679351(v=vs.85).aspx)
     * to retreive the last error message as shown here:
     * https://msdn.microsoft.com/de-de/library/windows/desktop/ms680582(v=vs.85).aspx
     * @return last error string
     */
    std::wstring getLastErrorString();
}
}

#endif // WINTOOLS_HPP
