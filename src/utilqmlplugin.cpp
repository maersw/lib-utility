/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "utilqmlplugin.hpp"

#include <QtQml>

#include "keyboardSimulator/util_keyboardsimulatorcomponent.hpp"
#include "logging/logcomponent.hpp"

LibUtilQmlPlugin::LibUtilQmlPlugin()
{
    
}

void LibUtilQmlPlugin::registerTypes(const char *uri)
{
    registerTypesStatic(uri);
}

void LibUtilQmlPlugin::registerTypesStatic(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("Lib.Util"));
#if LIBUTIL_KEYBOARDSIMULATOR == 1
    qmlRegisterType<libUtil::KeyboardSimulatorComponent>(uri, 1, 0, "KeyboardSimulator");
#endif
    
    qmlRegisterType<libUtil::LogComponent>(uri, 1, 0, "LogComponent");
}
