/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "util_keyboardsimulator.hpp"


#ifdef __linux__
    #include <X11/Xlib.h>
    #include <X11/keysym.h>
    #include <X11/extensions/XTest.h>
#elif defined _WIN32
    #include <windows.h>
#else

#endif

#include <iostream>
#include <memory>
#include <vector>

namespace libUtil
{
KeyboardSimulator::KeyboardSimulator()
{
    
}
#ifdef __linux__
void KeyboardSimulator::sendChar(char c)
{
//    // https://stackoverflow.com/questions/1262310/simulate-keypress-in-a-linux-c-console-application
//    // https://www.c-plusplus.net/forum/224689-full
//    Display *display = XOpenDisplay(NULL);
//    XKeyEvent event;
//    event.display = display;
//    event.root = DefaultRootWindow(display);

//    int revert = 0;
//    XGetInputFocus(display, &event.window, &revert);

//    event.subwindow = None;
//    event.same_screen = true;

//    event.state = isupper(c) ? ShiftMask : 0;
//    event.keycode = XKeysymToKeycode(event.display, c);

//    event.type = KeyPress;
//    event.time = CurrentTime;
//    XSendEvent(event.display, event.window, true, KeyPressMask, reinterpret_cast<XEvent *>(&event));

//    event.type = KeyRelease;
//    event.time = CurrentTime;
//    XSendEvent(event.display, event.window, true, KeyReleaseMask, reinterpret_cast<XEvent *>(&event));



//    if (isupper(c))
//    {

//    }

//    Display *display;
//    const char str[] = {c, 0};

//    unsigned int keycode = XStringToKeysym(str);

//    keycode = XKeysymToKeycode(display, keycode);
//    display = XOpenDisplay(NULL);

//    //keycode = XKeysymToKeycode(display, XK_Pause);
//    XTestFakeKeyEvent(display, keycode, True, 0);
//    XTestFakeKeyEvent(display, keycode, False, 0);
//    XFlush(display);


}

void KeyboardSimulator::sendAltTab()
{

}


#elif defined _WIN32

void KeyboardSimulator::sendChar(char c)
{    
    // \r\n seems to require special treatment to work also over remote
    // desktop connections...
    if (c == '\r')
    {
        // ignore
    }
    else if (c == '\n' || c == '\t')
    {   
        // https://stackoverflow.com/questions/16290592/sendinput-to-remote-desktop
        
        // https://msdn.microsoft.com/de-de/library/windows/desktop/ms646296(v=vs.85).aspx
        HKL keyboardLayout = GetKeyboardLayout(0);
        
        // https://msdn.microsoft.com/de-de/library/windows/desktop/ms646332(v=vs.85).aspx
        SHORT scan = VkKeyScanEx(c, keyboardLayout);
        BYTE vk = scan & 0x00FF;
        //BYTE shiftState = (scan & 0xFF00) >> 8;
        
        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms646306(v=vs.85).aspx
        UINT mapped = MapVirtualKey(vk, MAPVK_VK_TO_VSC);

        INPUT ins[2];
        
        { // key down
            ins[0].type = INPUT_KEYBOARD;
            ins[0].ki.wVk = 0;
            ins[0].ki.wScan = mapped;
            ins[0].ki.dwFlags = KEYEVENTF_SCANCODE;
        }
        
        { // key up
            ins[1].type = INPUT_KEYBOARD;
            ins[1].ki.wVk = 0;
            ins[1].ki.wScan = mapped;
            ins[1].ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;
        }
        
        SendInput(sizeof(ins) / sizeof(INPUT), ins, sizeof(INPUT));
    }
    else
    {
        // https://stackoverflow.com/questions/1138606/alternative-to-sendkeys-when-running-over-remote-desktop
        // https://msdn.microsoft.com/en-us/library/ms646310(VS.85).aspx
        // https://stackoverflow.com/questions/3554171/cant-send-a-single-key-function-to-remote-desktop#20829448    
        INPUT ins[2];
        ins[0].type = INPUT_KEYBOARD;
        ins[0].ki.wVk = 0;
        ins[0].ki.wScan = c;
        ins[0].ki.dwFlags = KEYEVENTF_UNICODE;
        
        ins[1].type = INPUT_KEYBOARD;
        ins[1].ki.wVk = 0;
        ins[1].ki.wScan = c;
        ins[1].ki.dwFlags = KEYEVENTF_UNICODE | KEYEVENTF_KEYUP;
        
        SendInput(sizeof(ins) / sizeof(INPUT), ins, sizeof(INPUT));
    }
}

void KeyboardSimulator::sendAltTab()
{
    INPUT ins[4];

    int i = 0;

    { // alt key down
        ins[i].type = INPUT_KEYBOARD;
        ins[i].ki.wVk = VK_MENU;
        ins[i].ki.wScan = 0;
        ins[i].ki.dwFlags = 0;
    }

    i = 1;
    { // tab key down
        ins[i].type = INPUT_KEYBOARD;
        ins[i].ki.wVk = VK_TAB;
        ins[i].ki.wScan = 0;
        ins[i].ki.dwFlags = 0;
    }

    i= 2;
    { // tab key up
        ins[i].type = INPUT_KEYBOARD;
        ins[i].ki.wVk = VK_TAB;
        ins[i].ki.wScan = 0;
        ins[i].ki.dwFlags = KEYEVENTF_KEYUP;
    }

    i = 3;
    { // alt key up
        ins[i].type = INPUT_KEYBOARD;
        ins[i].ki.wVk = VK_MENU;
        ins[i].ki.wScan = 0;
        ins[i].ki.dwFlags = KEYEVENTF_KEYUP;
    }

    SendInput(sizeof(ins) / sizeof(INPUT), ins, sizeof(INPUT));
}
#else

#endif

void KeyboardSimulator::sendString(const std::string &string)
{
    for (auto iter = string.begin(); iter != string.end(); ++iter)
    {
        sendChar(*iter);
    }    
}



} // end namespace libUtil

