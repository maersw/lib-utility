/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#ifndef KEYBOARDSIMULATORCOMPONENT_HPP
#define KEYBOARDSIMULATORCOMPONENT_HPP

#include <QObject>

#include "util_keyboardsimulator.hpp"

namespace libUtil
{

class KeyboardSimulatorComponent : public QObject
{
    Q_OBJECT
public:
    explicit KeyboardSimulatorComponent(QObject *parent = nullptr);
    Q_INVOKABLE void focusLastWindow(quint32 delayMs);
    Q_INVOKABLE void sendString(const QString & str);
signals:
    
public slots:
    
private:
    KeyboardSimulator m_simulator;
};

} // end namespace libUtil

#endif // KEYBOARDSIMULATORCOMPONENT_HPP
