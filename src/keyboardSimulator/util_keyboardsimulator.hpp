/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#ifndef KEYBOARDSIMULATOR_HPP
#define KEYBOARDSIMULATOR_HPP

#include <string>
namespace libUtil
{

class KeyboardSimulator
{
public:
    KeyboardSimulator();
    
    void sendChar(char c);
    void sendString(const std::string & string);
    
    void sendAltTab();
};

} // end namespace libUtil


#endif // KEYBOARDSIMULATOR_HPP
