/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "util_keyboardsimulatorcomponent.hpp"

#include <QThread>

namespace libUtil
{

KeyboardSimulatorComponent::KeyboardSimulatorComponent(QObject *parent) : QObject(parent)
{
    
}

void KeyboardSimulatorComponent::focusLastWindow(quint32 delayMs)
{
    m_simulator.sendAltTab();
    
    // very dirty hack: wait for last window showing up
    QThread::currentThread()->usleep(1000 * delayMs);
}

void KeyboardSimulatorComponent::sendString(const QString &str)
{
    m_simulator.sendString(str.toStdString());
    
}

} // end namespace libUtil
