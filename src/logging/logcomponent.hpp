/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#ifndef LOGCOMPONENT_HPP
#define LOGCOMPONENT_HPP

#include <QQuickItem>
#include <QLoggingCategory>
#include <QSharedPointer>

namespace libUtil
{
/**
 * @brief The LogComponent class provides a QML component
 * for writing log messages to a QLoggingCategory.
 * 
 * Example
@code{.cpp}
qmlRegisterType<LogComponent>("Foo.Bar", 1, 0, "LogComponent");
@endcode

@code{.qml}
import QtQuick 2.0
import Foo.Bar 1.0

Item{
    LogComponent{
        id: log
        name: "category.qml"
    }
    
    Timer{
        running: true
        interval: 1000
        repeat: true
        onTriggered: {
            log.info("Timer has elapsed");
        }
    }
}
@endcode
 */
class LogComponent : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    
public:
    explicit LogComponent(QQuickItem *parent = nullptr);
    
    /**
     * @brief name
     * @return Name of logging category
     */
    QString name() const;
    
    /**
     * @brief setName Sets the name of the logging component.
     * Usually this should be called just once. Updating the name
     * will create a new logging category, destroying the old one.
     * @param name
     */
    void setName(const QString &name);
    
    // QQmlParserStatus interface
    virtual void componentComplete() override;
    
signals:
    void nameChanged();
    
public slots:
    /**
     * @brief debug Write a debug message
     * @param message
     */
    void debug(const QString & message);
    
    /**
     * @brief info Write an info message
     * @param message
     */
    void info(const QString & message);
    
    /**
     * @brief warning Write a warning message
     * @param message
     */
    void warning(const QString & message);
    
    /**
     * @brief critical Write a critical message
     * @param message
     */
    void critical(const QString & message);
protected:
    
private:
    QSharedPointer<QLoggingCategory> m_lg;
    
    QByteArray m_name;
    
};

} // end namespace libUtil

#endif // LOGCOMPONENT_HPP
