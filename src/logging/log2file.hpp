#ifndef LOG2FILE_HPP
#define LOG2FILE_HPP

#include <QtGlobal>
#include <QFile>
#include <QSharedPointer>

namespace libUtil
{

class Log2File
{
public:
    static void install(const QString & fileName);
private:
    
    static QtMessageHandler m_originalMessageHandler;
    static QSharedPointer<QFile> m_logFile;
    
    static void handleLogMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    
    
};

} // end namespace libUtil

#endif // LOG2FILE_HPP
