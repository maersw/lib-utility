#include "log2file.hpp"

#include <QMutex>
#include <QMutexLocker>
#include <QTextStream>

namespace libUtil
{
QtMessageHandler Log2File::m_originalMessageHandler = Q_NULLPTR;
QSharedPointer<QFile> Log2File::m_logFile;

void Log2File::install(const QString & fileName)
{
    if (m_logFile.isNull())
    {
        m_logFile = QSharedPointer<QFile>(new QFile(fileName));
        
        if (m_logFile->open(QFile::WriteOnly))
        {
            m_originalMessageHandler = qInstallMessageHandler(&Log2File::handleLogMessage);
        }
    }
}

void Log2File::handleLogMessage(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Call original message handler first
    if (m_originalMessageHandler)
    {
        m_originalMessageHandler(type, context, msg);
    }

    {        
        // This function might be called from different threads.
        // => Lock to prevent mixed file content
        static QMutex mutex;
        
        QMutexLocker lock(&mutex);
        
        QTextStream ts(m_logFile.data());
        
        ts<<type<<" "<<context.category<<" "<<msg<<endl;
    }
}




} // end namespace libUtil
