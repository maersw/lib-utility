/*************************************************************************
 * Copyright (C) 2017 Martin Ertl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/
#include "logcomponent.hpp"

Q_LOGGING_CATEGORY(foo, "foo")

namespace libUtil
{

LogComponent::LogComponent(QQuickItem *parent) : 
    QQuickItem(parent)
{
    
}

QString LogComponent::name() const
{
    return m_name;
}

void LogComponent::setName(const QString &name)
{
    if (m_lg)
    {
        warning("Changing category name to: " + name);
    }
    
    // destroy potentially existing logging category first
    // because the name will become invalid after changing m_name
    m_lg = QSharedPointer<QLoggingCategory>(Q_NULLPTR);
    m_name = name.toLatin1();
    m_lg = QSharedPointer<QLoggingCategory>(new QLoggingCategory(m_name.constData()));

}

void LogComponent::componentComplete()
{
    QQuickItem::componentComplete();
    
}

void LogComponent::debug(const QString &message)
{
    if (m_lg && m_lg->isDebugEnabled())
    {
        QMessageLogger("", -1, "", m_lg->categoryName()).debug()<<message;
    }        
}

void LogComponent::info(const QString &message)
{
    if (m_lg && m_lg->isInfoEnabled())
    {
        QMessageLogger("", -1, "", m_lg->categoryName()).info()<<message;
    }   
}

void LogComponent::warning(const QString &message)
{
    if (m_lg && m_lg->isWarningEnabled())
    {
        QMessageLogger("", -1, "", m_lg->categoryName()).warning()<<message;
    }   
}

void LogComponent::critical(const QString &message)
{
    if (m_lg && m_lg->isCriticalEnabled())
    {
        QMessageLogger("", -1, "", m_lg->categoryName()).critical()<<message;
    }   
}


} // end namespace libUtil
